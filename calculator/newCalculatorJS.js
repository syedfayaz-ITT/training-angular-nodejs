var text;

function getInputFromTextbox(element) {
    return document.getElementById(element);
}

function setDisplay(val) {
    if (typeof val == "undefined") {
        getInputFromTextbox("expression").value = "";
    } else if (typeof val === "boolean") {
        getInputFromTextbox("expression").value = "Incorrect Use of < or >";
        setTimeout(function() {
            getInputFromTextbox("expression").value = "";
        }, 400);
    } else {
        getInputFromTextbox("expression").value = val;
    }
}

function clearField(val) {
    getInputFromTextbox("expression").value = val;
}

function addToExpression(val) {
    getInputFromTextbox("expression").value += val;
}

function evaluateExpression(e) {
    try {
        setDisplay(eval(getInputFromTextbox("expression").value)); //USING eval() function for arithmetic operations.
    } catch (e) {
        var errorType = document.getElementById("expression").value;
        if (errorType.indexOf("++") !== -1) {
            setDisplay("Multiple +");
        } else if (errorType.indexOf("--") !== -1) {
            setDisplay("Multiple -");
        } else if (errorType.indexOf("**") !== -1) {
            setDisplay("Multiple *");
        } else if (errorType.indexOf("//") !== -1) {
            setDisplay("Multiple /");
        } else {
            clearField("Invalid input");
        }
        setTimeout(function() {
            getInputFromTextbox("expression").value = "";
        }, 400);
    }
}

function checkInput(e) {
    var e = e || event,
        char =
        e.type == "keypress" ?
        String.fromCharCode(e.keyCode || e.which) :
        (e.clipboardData || window.clipboardData).getData("Text");
    if (/[^0-9\-\/\*\<\>\+\.%]/gi.test(char)) {
        return false;
    }
}

function checkKey(event) {
    text = document.getElementById("expression");
    text.onkeypress = checkInput;
    text.onpaste = checkInput;
    // Check for ENTER key
    if (event.keyCode == 13) {
        // Check for Ctrl key
        evaluateExpression();
    } else if (event.keyCode == 17) {
        clearField("");
        //checkInput(event);
    }
}