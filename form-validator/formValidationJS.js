function validate() {

	var name=document.getElementById("name").value;
	var email=document.getElementById("email").value;
	var mobileNo=document.getElementById("mobileNo").value;
	var country=document.getElementById("country").value;
	var zipcode=document.getElementById("zipcode").value;
	var atPosition = email.indexOf("@");
    var dotPosition = email.lastIndexOf(".");
    var letters = /^[A-Za-z]+$/; 
    var numbers=/^[0-9]+$/;

	if(name=="" || !name.match(letters)) {
		alert("Enter Correct Name");
		//document.getElementById("demo").innerHTML="Enter Correct Name";
		return false;
	}
		
    if(email=="" || atPosition < 1 || ( dotPosition - atPosition < 2 )) {
        alert("Enter Correct Email ID");
        return false;
    }
	
	if(mobileNo.length!=10 || mobileNo =="" || !mobileNo.match(numbers)) {
		alert("Enter Correct Mobile Number [10 Digits]");
		return false;
	}

	if(country=="" || !country.match(letters)) {
		alert("Enter Correct Country");
		return false;
	}

	if(zipcode=="" || zipcode.length!=6 || !zipcode.match(numbers)) {
		alert("Enter Correct Zipcode [6 Digits]");
		return false;
	}
	return true;

}

function clearFields() {
	document.getElementById("name").value="";
	document.getElementById("email").value="";
	document.getElementById("mobileNo").value="";
	document.getElementById("country").value="";
	document.getElementById("zipcode").value="";
}