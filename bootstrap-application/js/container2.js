
function convertFromUTCTime(offset22) {

    // create Date object for current location
    //dateObject = new Date();
    
    // convert to msec
    // add local time zone offset 
    // get UTC time in msec
    //utcTime = dateObject.getTime() + (dateObject.getTimezoneOffset() * 60000);
   // console.log("UTC TIME : "+utcTime);
    
    var date22 = new Date(document.getElementById("manualTimestamp22").value);    //////ADDED EXTRA
    var utcTime22 = date22.getTime()+(document.getElementById("mySelect33").value * 60000);   /////ADDED EXTRA
    console.log("UTC TIME 2: "+utcTime22);

    // create new Date object for different city
    // using supplied offset
    newDate22 = new Date(utcTime22 + (3600000*offset22));
    console.log("NEW DATE 2: "+ newDate22);
    
    return newDate22.toLocaleString();

}

function myFunction22() {
   // var selectedRegion11=document.getElementById('mySelect33').value;
    var selectedRegion22=document.getElementById('mySelect22').value;
    console.log(selectedRegion22);
    var timeInRegion22;
    timeInRegion22=convertFromUTCTime(selectedRegion22);
    document.getElementById("para22").innerHTML=timeInRegion22;
}

function enableSelect() {
    if(this.value=='01') {
        document.getElementById("mySelect22").disabled=true;
    }
    else {
        document.getElementById("mySelect22").disabled=false;
    }
}
