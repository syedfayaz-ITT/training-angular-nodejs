var text;

function getInputFromTextbox(element) {
    return document.getElementById(element);
}



function clearField(val) {

    if (typeof val == 'undefined') {
        getInputFromTextbox("expression").value = "";
    } else {
        getInputFromTextbox("expression").value = val;
    }
}

function addToExpression(val) {
    getInputFromTextbox("expression").value += val;
}


function evaluateExpression(e) {

    try {
        clearField(eval(getInputFromTextbox("expression").value));
    } catch (e) {
        if (e instanceof SyntaxError) {
            clearField(e.message);
            console.log(e.name);
        } else {
            console.log(e.name);
            clearField("Invalid input");
            console.log(e.message);
        }
        //clearField('Incorrect Input') ;
        setTimeout( function() {
            getInputFromTextbox("expression").value = ""
        }, 400);
    }
}


function checkInput(e) {

    var e = e || event,
        char = e.type == 'keypress' ? String.fromCharCode(e.keyCode || e.which) : (e.clipboardData || window.clipboardData).getData('Text');
    if (/[^0-9\-\/\*\<\>\+\.%]/gi.test(char)) {
        return false;
    }
}


function checkKey(event) {
    text = document.getElementById('expression');
    text.onkeypress = checkInput;
    text.onpaste = checkInput;
    if (event.keyCode == 13) { // Check for ENTER key
        evaluateExpression();
    } else if (event.keyCode == 17) { // Check for Ctrl key
        clearField("");
    }
}