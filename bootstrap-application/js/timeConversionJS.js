
// function to calculate local time
// in a different city
// given the city's UTC offset
function convertFromLocalTime(offset) {

    // create Date object for current location
    dateObject = new Date();
    
    // convert to msec
    // add local time zone offset 
    // get UTC time in msec
    //1000 milliseconds = 1 second, and 1 minute = 60 seconds.Hence 60 * 1000 = 60000 milliseconds
    utcTime = dateObject.getTime() + (dateObject.getTimezoneOffset() * 60000); //Converting minutes to milliseconds
    console.log("UTC TIME : "+utcTime);
    
    // create new Date object for different city using supplied offset
    //1000 millseconds = 1 second, and 1 hour = 3600 seconds. Therefore 3600 * 1000 = 3600000 milliseconds
    newDate = new Date(utcTime + (3600000*offset)); //Converting hours to milliseconds
    console.log("NEW DATE : "+newDate);

    return newDate.toLocaleString(); //Converting milliseconds to proper DATE and TIME FORMAT

}

function myFunction() {
    var selectedRegion=document.getElementById('mySelect').value;
    console.log(selectedRegion);
    var timeInRegion;
    timeInRegion=convertFromLocalTime(selectedRegion);
    document.getElementById("para").innerHTML=timeInRegion;
    document.getElementById("para1").innerHTML=convertFromLocalTime('+5.5');
}
