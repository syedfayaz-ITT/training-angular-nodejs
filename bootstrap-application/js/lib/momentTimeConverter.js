$(window).ready(function() {

    var timezone = moment.tz.names(); //GET CONTINENT/REGION NAMES USING MOMENT.JS LIBRARY
    var localTimeStamp = new Date(); //CREATE LOCAL TIMESTAMP OBJECT
    document.getElementById("timeinIndia").innerHTML = localTimeStamp.toLocaleString(); //PRINTING LOCAL TIME
    document.getElementById("localContinent").innerHTML = moment.tz.guess();
    getValues("#localSelect");
    getValues("#manualSelectStatic");
    getValues("#manualSelectDynamic");

    
    function getValues(select) {

        for (var i = 0; i < timezone.length; i++) {
            $(select).append('<option value="' + timezone[i] + '">' + timezone[i] + '</option>');
        }
    }


    $("#localSelect").on('change', function() {
        var selectedRegion, offsetValue;
        selectedRegion = this.value;
        document.getElementById("timeZoneofSelectedRegion").innerHTML = "GMT : " + moment().tz(selectedRegion).format('Z');
        offsetValue = moment().tz(selectedRegion).format("Z").replace(":", ".");
        console.log("OFFSET VALUE : " + offsetValue);
        dateObject = new Date();
        utcTime = dateObject.getTime() + (dateObject.getTimezoneOffset() * 60000); //Converting minutes to milliseconds
        console.log("UTC TIME : " + utcTime);
        newDate = new Date(utcTime + (3600000 * offsetValue));
        console.log("NEW DATE : " + newDate);
        newRegion = newDate.toLocaleString();
        document.getElementById("timeCorrespondingLocal").innerHTML = newRegion;

    });

    $("#manualSelectDynamic").on('change', function() {

        var datefromInputBox = Date.parse(document.getElementById("inputForManualTimestamp").value);
        console.log("DATE : "+datefromInputBox);
        var selectedOptionFromStatic = document.getElementById("manualSelectStatic").value;
        var offsetOfSelectedOption = moment().tz(selectedOptionFromStatic).format("Z");
    

        var selectedOptionFromDynamic = document.getElementById('manualSelectDynamic').value;
        var offsetInHours = moment().tz(selectedOptionFromDynamic).format("Z");
        var offsetFormatInHours = offsetInHours.replace(":", ".");

  
        var selectedTimezoneOffset=moment.tz.zone(selectedOptionFromStatic).offset(datefromInputBox);
        var utcTimeforManual = datefromInputBox + (selectedTimezoneOffset * 60000);
        console.log("UTC TIME: " + utcTimeforManual);
        // create new Date object for different city
        // using supplied offset
        var newManualTimestamp = new Date(utcTimeforManual + (3600000 * offsetFormatInHours));
        console.log("NEW DATE: " + newManualTimestamp);
        var timeInManualRegion = newManualTimestamp.toLocaleString();
        document.getElementById("timeCorrespondingManual").innerHTML = timeInManualRegion;

    });
   
});