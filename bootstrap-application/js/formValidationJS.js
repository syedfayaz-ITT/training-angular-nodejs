function validate() {

    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var mobileNo = document.getElementById("mobileNo").value;
    var country = document.getElementById("country").value;
    var zipcode = document.getElementById("zipcode").value;
    var atPosition = email.indexOf("@");
    var dotPosition = email.lastIndexOf(".");
    var letters = /^[A-Za-z]+$/;
    var numbers = /^[0-9]+$/;

    if (name == "" || !name.match(letters)) {
        document.getElementById("nameError").innerHTML = "Enter Correct Name";
        return false;
    }

    if (email == "" || atPosition < 1 || (dotPosition - atPosition < 2)) {
        document.getElementById("emailError").innerHTML = "Enter correct Email ID";
        return false;
    }

    if (mobileNo.length != 10 || mobileNo == "" || !mobileNo.match(numbers)) {
        document.getElementById("mobileNoError").innerHTML = "Enter correct Mobile Number [10 Digits]";
        return false;
    }

    if (country == "" || !country.match(letters)) {
        document.getElementById("countryError").innerHTML = "Enter correct Country";
        return false;
    }

    if (zipcode == "" || zipcode.length != 6 || !zipcode.match(numbers)) {
        document.getElementById("zipcodeError").innerHTML = "Enter correct Zipcode [6 digits]";
        return false;
    }
    return true;

}

function clearFields() {
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("mobileNo").value = "";
    document.getElementById("country").value = "";
    document.getElementById("zipcode").value = "";
    document.getElementById("nameError").innerHTML = "";
    document.getElementById("emailError").innerHTML = "";
    document.getElementById("mobileNoError").innerHTML = "";
    document.getElementById("countryError").innerHTML = "";
    document.getElementById("zipcodeError").innerHTML = "";
}