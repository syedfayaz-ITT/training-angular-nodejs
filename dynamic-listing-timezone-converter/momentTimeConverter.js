$(document).ready(function() {
    var timezone = moment.tz.names(); //GET CONTINENT/REGION NAMES USING MOMENT.JS LIBRARY
    var localTimeStamp = new Date(); //CREATE LOCAL TIMESTAMP OBJECT
    document.getElementById("timeinIndia").innerHTML = localTimeStamp.toLocaleString(); //PRINTING LOCAL TIME
    document.getElementById("localContinent").innerHTML = moment.tz.guess();

    var selectedRegion, offsetValue;

    for (var i = 0; i < timezone.length; i++) {
        $("#localSelect").append('<option value="' + timezone[i] + '">' + timezone[i] + '</option>');
    }
    $("#localSelect").selectpicker();



    $("#localSelect").on('change', function() {
        selectedRegion = this.value;
        document.getElementById("timeZoneofSelectedRegion").innerHTML = moment().tz(selectedRegion).format('Z');
        offsetValue = moment().tz(selectedRegion).format("Z").replace(":", ".");
        console.log("OFFSET VALUE : " + offsetValue);
        dateObject = new Date();
        utcTime = dateObject.getTime() + (dateObject.getTimezoneOffset() * 60000); //Converting minutes to milliseconds
        console.log("UTC TIME : " + utcTime);
        newDate = new Date(utcTime + (3600000 * offsetValue));
        console.log("NEW DATE : " + newDate);
        newRegion = newDate.toLocaleString();
        document.getElementById("timeCorrespondingLocal").innerHTML = newRegion;

    });



    for (var i = 0; i < timezone.length; i++) {

        $("#manualSelectStatic").append('<option value="' + timezone[i] + '">' + timezone[i] + '</option>');
    }
    $("#manualSelectStatic").selectpicker();



    $("#manualSelectDynamic").on('change', function() {
        var newDate22, timeInRegion22, offsetFormat22, selectedRRR, offsetVal22, offsetFormat22;

        var selectedRegion22 = document.getElementById('manualSelectDynamic').value;
        offsetVal111 = moment().tz(selectedRegion22).format("Z");
        offsetFormat111 = offsetVal111.replace(":", ".");

        var date22 = new Date(document.getElementById("inputForManualTimestamp").value); //////ADDED EXTRA

        var mmm = document.getElementById("manualSelectStatic").value;
        offset333 = moment().tz(mmm).format("Z");
        offsetFormat333 = offset333.replace(":", ".");
        var utcTime22 = date22.getTime() + (offsetFormat333 * 60000); /////ADDED EXTRA
        console.log("UTC TIME 2: " + utcTime22);
        selectedRRR = this.value;
        offsetVal22 = moment().tz(selectedRRR).format("Z");
        offsetFormat22 = offsetVal22.replace(":", ".");
        // create new Date object for different city
        // using supplied offset
        newDate22 = new Date(utcTime22 + (3600000 * offsetFormat22));
        console.log("NEW DATE 2: " + newDate22);
        timeInRegion22 = newDate22.toLocaleString();
        document.getElementById("timeCorrespondingManual").innerHTML = timeInRegion22;

    });

    for (var i = 0; i < timezone.length; i++) {

        $("#manualSelectDynamic").append('<option value="' + timezone[i] + '">' + timezone[i] + '</option>');
    }
    $("#manualSelectDynamic").selectpicker();

});