
myApp.controller("CartController", function($scope, kartService) {
  $scope.kart = kartService.getKart();
  
  $scope.buy = function(item) {
    //console.log("item: ", item);
    kartService.buy(item);
  }
});

myApp.controller("HeaderController", function($scope, $location) {
  $scope.appDetails = {};
  $scope.appDetails.title = "Buy and Sell";
  $scope.appDetails.tagline = "Sell used products";
  
  $scope.nav = {};
  $scope.nav.isActive = function(path) {
    if (path === $location.path()) {
      return true;
    }
    
    return false;
  }
});

myApp.controller("ShopController", function($scope, bookService, kartService) {
  $scope.items = bookService.getBooks();
  
  $scope.addToKart = function(item) {
    kartService.addToKart(item);
  }
});

myApp.controller("SellController", function($scope, bookService, kartService) {
  $scope.msg = "SELL"
});