
myApp.factory("kartService", function() {
  var kart = [];
  
  return {
    getKart: function() {
      return kart;
    },
    addToKart: function(item) {
      kart.push(item);
    },
    buy: function(item) {
      alert("Thanks for buying: " + item.name);
    }
  }
});

myApp.factory("bookService", function() {
  var items = [
    {
      imgUrl: "laptop.jpg",
      name: "HP Pavilion Laptop",
      price: 16000,
      details: "3rd Gen Intel Core i5 processor | 8 GB DDR3 RAM | Windows 8 | 15.6 HD Screen | Island Style backlit Keyboard"
    },
    {
      imgUrl: "mouse.png",
      name: "Optical Mouse",
      price: 350,
      details: "HP Optical Scroll mouse | USB interface | 3 buttons with scroll wheel | Compatible to all notebook and Desktop"
    },
    {
      imgUrl: "charger.jpg",
      name: "HP Charger",
      price: 750,
      details: "Compatible with HP Compac series, Pavilion series"
    },
    {
      imgUrl: "headset.jpg",
      name: "Logitech Headsets",
      price: 599,
      details: "Full stereo feature for superior audio quality | Rotating microphone for better convenience"
    },
    {
      imgUrl: "bag.jpg",
      name: "HP Laptop Bag",
      price: 300,
      details: "Nylon laptop bag | 4 compartments | Waterproof"
    },
    {
      imgUrl: "watch.jpg",
      name: "Tommy Hilfiger Watch",
      price: 700,
      details: "Waterproof | Leather Band | Round case shape | Quartz movement"
    }
  ];
  
  return {
    getBooks: function() {
      return items;
    },
    addToKart: function(item) {
      
    }
  }
});