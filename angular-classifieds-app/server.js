'use strict';
// ----- initialization
var express = require('express');
var app = express();

app.use(express.static('classifieds-app'));

// ----- Routes
app.get('/',function(req, res){
   res.send('Node Server');
});

// ----- Server
var server = app.listen(3000, function () {
  
  var port = server.address().port

  console.log('Classified App listening on port 3000! go to http://localhost:%s',  port)
})